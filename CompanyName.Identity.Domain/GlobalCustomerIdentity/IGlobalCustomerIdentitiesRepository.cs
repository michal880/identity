﻿namespace CompanyName.Identity.Domain
{
    public interface IGlobalCustomerIdentitiesRepository
    {
        void Create(GlobalCustomerIdentityEntity entity);
    }
}
