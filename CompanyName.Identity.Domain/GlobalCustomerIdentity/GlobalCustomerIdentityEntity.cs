﻿using System;

namespace CompanyName.Identity.Domain
{
    public class GlobalCustomerIdentityEntity
    {
        public Guid Id { get; }

        public GlobalCustomerIdentityEntity(Guid id)
        {
            Id = id;
        }
    }
}
