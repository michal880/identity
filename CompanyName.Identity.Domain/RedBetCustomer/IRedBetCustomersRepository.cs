﻿using System;

namespace CompanyName.Identity.Domain
{
    public interface IRedBetCustomersRepository
    {
        void Create(RedBetCustomerEntity entity);
        MrGreenCustomerEntity Get(Guid id);
        void Update(RedBetCustomerEntity entity);
        void Remove(Guid id);
    }
}
