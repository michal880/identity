﻿using System;

namespace CompanyName.Identity.Domain
{
    public sealed class RedBetCustomerEntity
    {
        public RedBetCustomerEntity(Guid id, Guid globalCustomerId, string firstName, string lastName, string address, string favoriteFootballTeam)
        {
            Id = id;
            GlobalCustomerId = globalCustomerId;
            FirstName = firstName;
            LastName = lastName;
            Address = address;
            FavoriteFootballTeam = favoriteFootballTeam;
        }
        public Guid Id { get; }

        public Guid GlobalCustomerId { get; private set; }
        public string FirstName { get; private set; }
        public string LastName { get; private set; }
        public string Address { get; private set; }
        public string FavoriteFootballTeam { get; private set; }
    }
}
