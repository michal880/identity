﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CompanyName.Identity.Domain
{
    public sealed class MrGreenCustomerEntity
    {
        public MrGreenCustomerEntity(Guid id, Guid globalCustomerId, string firstName, string lastName, string address, string personalNumber)
        {
            Id = id;
            GlobalCustomerId = globalCustomerId;
            FirstName = firstName;
            LastName = lastName;
            Address = address;
            PersonalNumber = personalNumber;
        }

        public Guid Id { get; }
        public Guid GlobalCustomerId { get; private set; }
        public string FirstName { get; private set; }
        public string LastName { get; private set; }
        public string Address { get; private set; }
        public string PersonalNumber { get; private set; }
    }
}
