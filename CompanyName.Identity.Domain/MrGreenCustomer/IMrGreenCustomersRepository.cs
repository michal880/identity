﻿using System;

namespace CompanyName.Identity.Domain
{
    public interface IMrGreenCustomersRepository
    {
        void Create(MrGreenCustomerEntity entity);
        MrGreenCustomerEntity Get(Guid id);
        void Update(MrGreenCustomerEntity entity);
        void Remove(Guid id);
    }
}
