﻿using System;

namespace CompanyName.Identity.Query
{
    public class CustomerDto
    {
        public Guid GlobalCustomerId { get; set; }
    }
}