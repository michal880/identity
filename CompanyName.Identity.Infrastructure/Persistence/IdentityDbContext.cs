﻿using System;
using System.Collections.Generic;
using System.Text;
using CompanyName.Identity.Domain;
using Microsoft.EntityFrameworkCore;

namespace CompanyName.Identity.Infrastructure.Persistence
{
    public class IdentityDbContext : DbContext
    {
        public IdentityDbContext(DbContextOptions<IdentityDbContext> options) : base(options)
        {
        }
        public DbSet<MrGreenCustomerEntity> MrGreenCustomers { get; set; }
        public DbSet<RedBetCustomerEntity> RedBetCustomers { get; set; }

    }
}
