﻿using CompanyName.Identity.Application;

namespace CompanyName.Identity.Infrastructure.Persistence
{
    public class EfCoreUnitOfWork : IUnitOfWork
    {
        private readonly IdentityDbContext _identityDbContext;

        public EfCoreUnitOfWork(IdentityDbContext identityDbContext)
        {
            _identityDbContext = identityDbContext;
        }

        public void Commit()
        {
            _identityDbContext.SaveChanges();
        }
    }
}
