﻿using CompanyName.Identity.Domain;
using System;
using System.Linq;

namespace CompanyName.Identity.Infrastructure.Persistence
{
    public class MrGreenCustomersRepository : IMrGreenCustomersRepository
    {
        private readonly IdentityDbContext _identityDbContext;

        public MrGreenCustomersRepository(IdentityDbContext identityDbContext)
        {
            _identityDbContext = identityDbContext;
        }

        public void Create(MrGreenCustomerEntity entity)
        {
            _identityDbContext.MrGreenCustomers.Add(entity);
        }

        public MrGreenCustomerEntity Get(Guid id)
        {
            return _identityDbContext.MrGreenCustomers.Single(e => e.Id == id);
        }

        public void Update(MrGreenCustomerEntity entity)
        {
            _identityDbContext.MrGreenCustomers.Update(entity);
        }

        public void Remove(Guid id)
        {
            _identityDbContext.MrGreenCustomers.Remove(_identityDbContext.MrGreenCustomers.Single(e => e.Id == id));
        }
    }
}
