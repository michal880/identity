﻿using CompanyName.Identity.Application;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace CompanyName.Identity.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RedBetCustomersController : Controller
    {
        
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        
        [HttpPost]
        public void Post([FromBody]RegisterUnknownRedBetCustomerCommand command, [FromServices]ICommandHandler<RegisterUnknownRedBetCustomerCommand> handler)
        {
            handler.Handle(command);
        }
        
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
