﻿using Microsoft.AspNetCore.Mvc;

namespace CompanyName.Identity.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomersController : Controller
    {
        [HttpGet]
        public IActionResult GetCustomers()
        {
            // return response from query handler
            return Ok();
        }
    }
}