﻿using System;
using CompanyName.Identity.Domain;

namespace CompanyName.Identity.Application
{
    public sealed class RegisterUnknownRedBetCustomerCommandHandler : ICommandHandler<RegisterUnknownRedBetCustomerCommand>
    {
        private readonly IRedBetCustomersRepository _redBetCustomersRepository;
        private readonly IGlobalCustomerIdentitiesRepository _globalCustomerIdentitiesRepository;
        private readonly IUnitOfWork _unitOfWork;

        public RegisterUnknownRedBetCustomerCommandHandler(IRedBetCustomersRepository redBetCustomersRepository, IGlobalCustomerIdentitiesRepository globalCustomerIdentitiesRepository, IUnitOfWork unitOfWork)
        {
            _redBetCustomersRepository = redBetCustomersRepository;
            _globalCustomerIdentitiesRepository = globalCustomerIdentitiesRepository;
            _unitOfWork = unitOfWork;
        }
        public void Handle(RegisterUnknownRedBetCustomerCommand command)
        {
            var customerIdentity = new GlobalCustomerIdentityEntity(Guid.NewGuid());
            _globalCustomerIdentitiesRepository.Create(customerIdentity);

            var newRedBetCustomer = new RedBetCustomerEntity(Guid.NewGuid(), customerIdentity.Id, command.FirstName, command.LastName, command.Address, command.FavoriteFootballTeam);
            _redBetCustomersRepository.Create(newRedBetCustomer);
            _unitOfWork.Commit();

        }
    }
}
