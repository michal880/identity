﻿using System;

namespace CompanyName.Identity.Application
{
    public sealed class RegisterUnknownRedBetCustomerCommand : ICommand
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public string FavoriteFootballTeam { get; set; }
    }
}
