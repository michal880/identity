﻿namespace CompanyName.Identity.Application
{
    public interface IUnitOfWork
    {
        void Commit();
    }
}
