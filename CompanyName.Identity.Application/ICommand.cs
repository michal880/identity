﻿using System;

namespace CompanyName.Identity.Application
{
    public interface ICommand
    {
        Guid Id { get; }
    }
}
